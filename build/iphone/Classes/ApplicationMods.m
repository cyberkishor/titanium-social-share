/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2015 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 *
 * WARNING: This is generated code. Do not modify. Your changes *will* be lost.
 */

#import "ApplicationMods.h"

@implementation ApplicationMods

+ (NSArray*) compiledMods
{
	NSMutableArray *modules = [NSMutableArray array];

	
		[modules addObject:[NSDictionary
			dictionaryWithObjectsAndKeys:@"social",
			@"name",
			@"dk.napp.social",
			@"moduleid",
			@"1.7.5",
			@"version",
			@"8152d7fc-6edb-4c40-8d6f-bc2cef87bc1a",
			@"guid",
			@"",
			@"licensekey",
			nil
		]];
		

	return modules;
}

@end